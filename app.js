let express = require('express');
let app = express();
let Pool = require('pg-pool');
let bodyParser = require('body-parser');
const url = require('url')
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
const params = url.parse(process.env.POSTGRES_URL);
const auth = params.auth.split(':');
 
const config = {
  user: auth[0],
  password: auth[1],
  host: params.hostname,
  port: params.port,
  database: params.pathname.split('/')[1]
};
const pool = new Pool(config);
app.get('/api/getuserinfo',(req,res) => {
    pool.connect((err,db) => {
        if(err) {
           return res.status(500).send({message : `internal server error in database ${err}`})
        } else {
            db.query("select * from role",(errs,resp) => {
                if(errs) {
                    
                   return res.status(404).send({message : `internal server error in database ${errs}`})
                } else{
                    return res.send(resp.rows)
                }
              
            })
        }
    })
})
app.get('/api/getstudent',(req,res) => {
    pool.connect((err,db) => {
        if(err) {
           return res.status(500).send({message : `internal server error in database ${err}`})
        } else {
            db.query("select * from student",(errs,resp) => {
                if(errs) {
                    
                   return res.status(404).send({message : `internal server error in database ${errs}`})
                } else{
                    return res.send(resp.rows)
                }
              
            })
        }
    })
})
app.get('/api/getclass',(req,res) => {
    pool.connect((err,db) => {
        if(err) {
           return res.status(500).send({message : `internal server error in database ${err}`})
        } else {
            db.query("select * from class",(errs,resp) => {
                if(errs) {
                    
                   return res.status(404).send({message : `internal server error in database ${errs}`})
                } else{
                    return res.send(resp.rows)
                }
              
            })
        }
    })
})
module.exports = app