FROM postgres:alpine
ENV POSTGRES_DB newdb
ADD ./init.sql /docker-entrypoint-initdb.d/